@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.silent.lib.util;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
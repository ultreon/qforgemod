@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.silent.lib.advancements;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;

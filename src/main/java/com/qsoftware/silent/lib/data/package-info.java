@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.silent.lib.data;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
package com.qsoftware.forgemod.common;

import net.minecraft.block.material.MaterialColor;

public interface IHasMaterialColor {
    MaterialColor getMaterialColor();
}

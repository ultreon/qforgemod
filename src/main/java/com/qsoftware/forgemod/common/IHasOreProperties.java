package com.qsoftware.forgemod.common;

import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.OreFeature;

public interface IHasOreProperties {
    OreProperties getOreProperties();
}

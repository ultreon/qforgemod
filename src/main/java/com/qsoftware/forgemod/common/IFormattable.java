package com.qsoftware.forgemod.common;

public interface IFormattable {
    String toFormattedString();
}

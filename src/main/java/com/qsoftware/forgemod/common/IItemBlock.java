package com.qsoftware.forgemod.common;

import net.minecraft.item.Item;

/**
 * Entity spawns class.
 *
 * @author Qboi123
 * @deprecated not in use.
 */
@Deprecated
public interface IItemBlock {
    Item toItem();
}

package com.qsoftware.forgemod.common;

import net.minecraft.item.DyeColor;

public interface IHasDyeColor {
    DyeColor getDyeColor();
}

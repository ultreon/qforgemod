package com.qsoftware.forgemod.objects.blocks.base;

import net.minecraft.block.WoodButtonBlock;

/**
 * Game PC block class.
 *
 * @author Qboi123
 * @deprecated Use {@link WoodButtonBlock} instead.
 */
@Deprecated
public class BaseWoodButtonBlock extends WoodButtonBlock {
    public BaseWoodButtonBlock(Properties properties) {
        super(properties);
    }
}

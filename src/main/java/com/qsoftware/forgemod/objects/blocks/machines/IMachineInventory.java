package com.qsoftware.forgemod.objects.blocks.machines;

import net.minecraft.inventory.IInventory;

public interface IMachineInventory extends IInventory {
    int getInputSlotCount();
}

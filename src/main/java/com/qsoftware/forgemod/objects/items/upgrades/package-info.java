@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.forgemod.objects.items.upgrades;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
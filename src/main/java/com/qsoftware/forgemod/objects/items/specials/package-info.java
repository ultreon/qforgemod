@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.forgemod.objects.items.specials;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.forgemod.objects.items.type;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
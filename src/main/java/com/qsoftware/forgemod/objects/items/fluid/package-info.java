@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.forgemod.objects.items.fluid;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
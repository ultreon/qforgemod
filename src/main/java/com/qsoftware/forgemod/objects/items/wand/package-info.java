@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.qsoftware.forgemod.objects.items.wand;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
package com.qsoftware.forgemod.groups;

/**
 * Groups collection.
 *
 * @author Qboi123
 */
public class Groups {
    public static final NatureItemGroup NATURE = NatureItemGroup.instance;
    //    public static final LeavesItemGroup LEAVES = LeavesItemGroup.instance;
//    public static final LogsItemGroup LOGS = LogsItemGroup.instance;
    public static final WoodItemGroup WOOD = WoodItemGroup.instance;
    public static final FurnitureItemGroup FURNITURE = FurnitureItemGroup.instance;
    public static final IngredientsItemGroup INGREDIENTS = IngredientsItemGroup.instance;
    public static final MiscellaneousItemGroup MISC = MiscellaneousItemGroup.instance;
    public static final SpawnEggsItemGroup SPAWN_EGGS = SpawnEggsItemGroup.instance;
    public static final OresItemGroup ORES = OresItemGroup.instance;
    public static final MetalCraftablesItemGroup METAL_CRAFTABLES = MetalCraftablesItemGroup.instance;
    public static final GemsItemGroup GEMS = GemsItemGroup.instance;
    public static final ToolsItemGroup TOOLS = ToolsItemGroup.instance;
    //    public static final ArmorsItemGroup ARMORS = ArmorsItemGroup.instance;
    public static final MachinesItemGroup MACHINES = MachinesItemGroup.instance;
    public static final SpecialsItemGroup SPECIALS = SpecialsItemGroup.instance;
    public static final ShapesItemGroup SHAPES = ShapesItemGroup.instance;
    public static final FletchingItemGroup FLETCHING = FletchingItemGroup.instance;
    public static final DungeonsItemGroup DUNGEONS = DungeonsItemGroup.instance;
    public static final RedstoneItemGroup REDSTONE = RedstoneItemGroup.instance;
    public static final FoodItemGroup FOOD = FoodItemGroup.instance;
    public static final FluidItemGroup FLUIDS = FluidItemGroup.instance;
    public static final BookshelfsItemGroup BOOKSHELFS = BookshelfsItemGroup.instance;
    public static final OverpoweredItemGroup OVERPOWERED = OverpoweredItemGroup.instance;
}

package com.qsoftware.forgemod.api.text;

public interface IHasTranslationKey {

    String getTranslationKey();
}
package com.qsoftware.forgemod.api.tier;

public interface ITier {

    BaseTier getBaseTier();
}
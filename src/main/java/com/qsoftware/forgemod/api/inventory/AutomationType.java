package com.qsoftware.forgemod.api.inventory;

public enum AutomationType {
    EXTERNAL,
    INTERNAL,
    MANUAL
}